¡Hola!

Si estás leyendo este texto, es porque encontraste la lata hacker en el RLab y estás viendo que hacer con ella.

<img src=https://gitlab.com/mar_fer/Pastillero/-/raw/main/Fotos/IMG_20231123_203013.jpg>

TL;DR: Es un desprendimiento del tamuy versátil, se puede utilizar para confeccionar un juego o usarlo como si fuera un teclado, tanto sea para enviar macros comoller de hardware hacking "Pixel" (Ministerio de Cultura de la Nación 2023). Se trata de una Raspberry Pi Pico conectada con 12 (doce) pulsadores y 12 (doce) LEDs rojos, junto con un buzzer. Siendo un microcontrolador  pulsaciones de teclas especiales, por ejemplo las de volumen, mute, etc.

Teclado Macropad:
El macropad está programado en CircuitPython (por la facilidad de uso de los protocolos HID)
La referencia de las teclas de ese proyecto está en 
https://docs.circuitpython.org/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode

Juego interactivo usando pulsadores y leds:
El Simon Game está progrmada en MicroPyhton y puede trasladarse con muy pocos ajustes a otros micros, por ejemplo ESP32 o ESP8266.

Se puede hacer un controlador midi (https://www.ianwootten.co.uk/2021/06/08/build-your-own-pico-powered-midi-controller-for-35/) pero hay que ajustar bastante ese código, puede ser que en una futura revisión salga!

Sobre la Pico hay soldado un pulsador extra a modo de reset, para no tener que andar retirando y moviendo el "débil" conector microUSB de la placa.

Resumen de conexiones:

Botón 1:  GP0
Botón 2:  GP1
Botón 3:  GP2
Botón 4:  GP3
Botón 5:  GP4
Botón 6:  GP5
Botón 7:  GP6
Botón 8:  GP7
Botón 9:  GP8
Botón 10:  GP9
Botón 11:  GP10
Botón 12:  GP11

led1:  GP13
led2:  GP14
led3:  GP16
led4:  GP17
led5:  GP18
led6:  GP19
led7:  GP28
led8:  GP27
led9:  GP26
led10:  GP22
led11:  GP21
led12:  GP20

Buzzer: GP25

                +----USB----+
 BOTON 1 > GP0  +           + vbus
 BOTON 2 > GP1  +           + vsys
           GND  +           + GND
 BOTON 3 > GP2  +           + 3v3_en
 BOTON 4 > GP3  +           + 3v3_out
 BOTON 5 > GP4  +           +
 BOTON 6 > GP5  +           + GP28 < LED 7
           GND  +           + GND
 BOTON 7 > GP6  +           + GP27 < LED 8
 BOTON 8 > GP7  +   #####   + GP26 < LED 9
 BOTON 9 > GP8  +   #####   + RUN             <--+ botón
BOTON 10 > GP9  +   #####   + GP22  < LED 10     #  de
           GND  +   #####   + GND             <--+ reset
BOTON 11 > GP10 +           + GP21 < LED 11
BOTON 12 > GP11 +           + GP20 < LED 12
           GP12 +           + GP19 < LED 6
   LED 1 > GP13 +           + GP18 < LED 5
           GND  +           + GND
   LED 2 > GP14 +           + GP17 < LED 4
  BUZZER > GP15 +           + GP16 < LED 3
                +-----------+
